<p align="center" >
  <img src="waypoints.png" title="logo" float=left>
</p>

# waypoints

#### 介绍
通过航点编辑器方便生成航点和编辑航点, 导出航点列表和导入航点列表

### Version 

<p align="center" >
  <img src="picture/waypoint_v1.0.3.png" title="version" float=left>
</p>


使用说明参考：

https://www.ncnynl.com/archives/202205/5246.html


#### 软件架构

```
ubuntu 20.04 
ros2 galactic
python3.8 
```



#### 安装教程

```
克隆整个仓库
git clone https://gitee.com/ncnynl/waypoints
cd waypoints
sudo chmod +x install.sh
./install.sh 

```


#### 使用教程

1. 直接运行

./waypoints

2. 在application界面搜索Waypoints, 点击图片即可.

3. 或者Waypoints图标点右键, Add to Favorites, 可以在左边固定显示图标

4. 保存的文件需要带后缀*.json. 当再加载时候就会自动显示.


