#!/usr/bin/env bash
#
# Description: Waypoints install script
#
# Copyright (C) 2022  <1043931@qq.com>
#
# URL: https://ncnynl.com
#


# system
if cat /etc/issue | grep -Eqi "ubuntu"; then
  release="ubuntu"
elif cat /proc/version | grep -Eqi "ubuntu"; then
  release="ubuntu"
else 
  echo -e "This script can not be run in your system now!" && exit 1
fi

# version
if [ $release == "ubuntu" ]; then
  version=$(awk -F'[= "]' '/VERSION_ID/{print $3}' /etc/os-release)
else
  echo -e "This script can not be run in your system now!" && exit 1
fi

# if [ $version == "18.04" ]; then
#   package="waypoints_v1.0.2_20220519_ubuntu18.04"
# el
if [ $version == "20.04" ]; then
  package="waypoints_v1.0.3_20220528_ubuntu20.04"
else
  echo -e "This script can not be run in your system now!" && exit 1
fi

echo -e "Start to install package $package to your system now!!!"

#build folder
sudo mkdir -p /usr/local/waypoints/
sudo cp $package /usr/local/waypoints/waypoints
sudo cp waypoints.png /usr/local/waypoints/waypoints.png
sudo chown -R $USER:$USER /usr/local/waypoints/
sudo ln -s /usr/local/waypoints/waypoints /usr/bin/waypoints

# copy waypoints.desktop to $USER/.local/share/applications
# can not run , *.desktop launch by root. will not load ~/.bashrc
# sudo cp waypoints.desktop $HOME/.local/share/applications/waypoints.desktop

echo -e "Finished"


